package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// Invader represents an enemy
type Invader struct {
	Sprite      *pixel.Sprite
	Pos         pixel.Matrix
	Strength    int
	Speed       float64
	Erraticness int // how often direction changes, the lower the more erractic
	FiringSpeed int // how often shooting happens, the lower the fastest
	Boundary    pixel.Rect
	xdir, ydir  bool
}

var rng *rand.Rand

func init() {
	rng = rand.New(rand.NewSource(time.Now().UnixNano()))
}

// NewInvader initializes a new invader
func NewInvader(imgPath string, strength int, speed float64, erraticness, firingspeed int, boundary pixel.Rect) (iv Invader, err error) {
	pic, err := loadPicture(imgPath)
	if err != nil {
		return
	}
	iv = Invader{
		Sprite:      pixel.NewSprite(pic, pic.Bounds()),
		Speed:       speed,
		Strength:    strength,
		Boundary:    boundary,
		Erraticness: erraticness,
		FiringSpeed: firingspeed,
		Pos:         pixel.IM,
	}
	// init random direction and position
	if rng.Intn(2)%2 == 0 {
		iv.xdir = true
	}
	if rng.Intn(2)%2 == 0 {
		iv.ydir = true
	}
	// initial position is random at the top of the window
	iv.Pos[4] = float64(rng.Intn(1000) % int(boundary.Max.X))
	iv.Pos[5] = boundary.Max.Y
	return
}

// MoveRandomly changes the position of an invader within the
// window boundary at a given speed.
func (iv *Invader) MoveRandomly() {
	// 1 chance in X to change direction randomly
	if rng.Intn(iv.Erraticness)%iv.Erraticness == 0 {
		iv.xdir = !iv.xdir
	}
	if rng.Intn(iv.Erraticness)%iv.Erraticness == 0 {
		iv.ydir = !iv.ydir
	}

	if iv.xdir {
		iv.Pos[4] += iv.Speed
		if iv.Pos[4] > iv.Boundary.Max.X-30 {
			// inverse x direction
			iv.xdir = false
			iv.Pos[4] -= iv.Speed
		}
	} else {
		iv.Pos[4] -= iv.Speed
		if iv.Pos[4] < iv.Boundary.Min.X+30 {
			// inverse x direction
			iv.xdir = true
			iv.Pos[4] += iv.Speed
		}
	}
	if iv.ydir {
		iv.Pos[5] += iv.Speed
		if iv.Pos[5] > iv.Boundary.Max.Y-30 {
			// inverse y direction
			iv.ydir = false
			iv.Pos[5] -= iv.Speed
		}
	} else {
		iv.Pos[5] -= iv.Speed
		if iv.Pos[5] < iv.Boundary.Min.Y+200 {
			// inverse y direction
			iv.ydir = true
			iv.Pos[5] += iv.Speed
		}
	}
}

// ShootRandomly will fire a bolt every once in a while
func (iv *Invader) ShootRandomly(win *pixelgl.Window) error {
	if rng.Intn(iv.FiringSpeed)%iv.FiringSpeed == 0 {
		fmt.Println("shoot")
		return Shoot(SmallBolt, iv.Pos, iv.Boundary, win)
	}
	return nil
}
