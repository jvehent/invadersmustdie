package main

import (
	"fmt"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// Bolt defines a bolt
type Bolt struct {
	Sprite   *pixel.Sprite
	Speed    float64
	Strength int
	Area     pixel.Rect
	Pos      pixel.Matrix
}

// Bolt sizes are defined as integer constants
const (
	SmallBolt int = iota
	MediumBolt
	LargeBolt
	MegaBolt
)

// Shoot creates a bolt and fires it. The bolt moves independently until
// it hits a target or exits the window
func Shoot(bolt int, pos pixel.Matrix, boundary pixel.Rect, win *pixelgl.Window) (err error) {
	var b Bolt
	switch bolt {
	case SmallBolt:
		pic, err := loadPicture("assets/red_laser.png")
		if err != nil {
			return err
		}
		b = Bolt{
			Sprite:   pixel.NewSprite(pic, pic.Bounds()),
			Strength: 1,
			Speed:    1,
			Area:     pixel.R(1, 1, 2, 2),
		}
	case MediumBolt:
		return fmt.Errorf("not implemented")
	case LargeBolt:
		return fmt.Errorf("not implemented")
	case MegaBolt:
		return fmt.Errorf("not implemented")
	}
	b.Pos = pos
	go func(b *Bolt, win *pixelgl.Window) {
		for b.Pos[5] > -50 {
			b.Move(win)
		}
	}(&b, win)
	return nil
}

// Move a bolt down the window
func (b *Bolt) Move(win *pixelgl.Window) {
	b.Pos[5] -= b.Speed
	b.Sprite.Draw(win, b.Pos)
}
